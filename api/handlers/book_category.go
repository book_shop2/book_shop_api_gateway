package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"api_gateway/api/http"
	"api_gateway/api/models"
	"api_gateway/genproto/catalog_service"
	"api_gateway/pkg/helper"
	"api_gateway/pkg/util"
)

// CreateBookCategory godoc
// @ID create_book_category
// @Router /book_category [POST]
// @Summary Create BookCategory
// @Description  Create BookCategory
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param profile body catalog_service.CreateBookCategory true "CreateBookCategoryRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.BookCategory} "GetBookCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBook_Category(c *gin.Context) {

	var book_category catalog_service.CreateBookCategory

	err := c.ShouldBindJSON(&book_category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.Book_CategoryService().Create(
		c.Request.Context(),
		&book_category,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetBookCategoryByID godoc
// @ID get_book_category_by_id
// @Router /book_category/{id} [GET]
// @Summary Get BookCategory  By ID
// @Description Get BookCategory  By ID
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=catalog_service.BookCategory} "BookCategoryBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBook_CategoryByID(c *gin.Context) {

	book_categoryID := c.Param("id")

	if !util.IsValidUUID(book_categoryID) {
		h.handleResponse(c, http.InvalidArgument, "book_category id is an invalid uuid")
		return
	}

	resp, err := h.services.Book_CategoryService().GetByID(
		context.Background(),
		&catalog_service.BookCategoryPrimaryKey{
			BookId: book_categoryID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetBook_CategoryList godoc
// @ID get_book_category_list
// @Router /book_category [GET]
// @Summary Get BookCategory s List
// @Description  Get Book_Category s List
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=catalog_service.GetListBookCategoryResponse} "GetAllBook_CategoryResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBook_CategoryList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.Book_CategoryService().GetList(
		context.Background(),
		&catalog_service.GetListBookCategoryRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateBook_Category godoc
// @ID update_book_category
// @Router /book_category/{id} [PUT]
// @Summary Update Book_Category
// @Description Update Book_Category
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body catalog_service.UpdateBookCategory true "UpdateBook_CategoryRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.BookCategoryList} "Book_Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBook_Category(c *gin.Context) {

	var book_categoryId catalog_service.UpdateBookCategory

	book_categoryId.BookId = c.Param("id")

	if !util.IsValidUUID(book_categoryId.BookId) {
		h.handleResponse(c, http.InvalidArgument, "book_id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&book_categoryId)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.Book_CategoryService().Update(
		c.Request.Context(),
		&book_categoryId,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchBook_Category godoc
// @ID patch_book_category
// @Router /book_category/{id} [PATCH]
// @Summary Patch Book_Category
// @Description Patch Book_Category
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.BookCategory} "Book_Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchBook_Category(c *gin.Context) {

	var updatePatchBook_Category models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchBook_Category)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchBook_Category.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchBook_Category.ID) {
		h.handleResponse(c, http.InvalidArgument, "book_category id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchBook_Category.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.Book_CategoryService().UpdatePatch(
		c.Request.Context(),
		&catalog_service.UpdatePatchBookCategory{
			BookId: updatePatchBook_Category.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteBook_Category godoc
// @ID delete_book_category
// @Router /book_category/{id} [DELETE]
// @Summary Delete Book_Category
// @Description Delete Book_Category
// @Tags BookCategory
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Book_Category data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBook_Category(c *gin.Context) {

	book_categoryId := c.Param("id")

	if !util.IsValidUUID(book_categoryId) {
		h.handleResponse(c, http.InvalidArgument, "book_category id is an invalid uuid")
		return
	}

	resp, err := h.services.Book_CategoryService().Delete(
		c.Request.Context(),
		&catalog_service.DeleteBookCategory{BookId: book_categoryId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
