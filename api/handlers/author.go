package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"api_gateway/api/http"
	"api_gateway/api/models"
	"api_gateway/genproto/catalog_service"
	"api_gateway/pkg/helper"
	"api_gateway/pkg/util"
)

// CreateAuthor godoc
// @ID create_author
// @Router /author [POST]
// @Summary Create Author
// @Description  Create Author
// @Tags Author
// @Accept json
// @Produce json
// @Param profile body catalog_service.CreateAuthor true "CreateAuthorRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Author} "GetAuthorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateAuthor(c *gin.Context) {

	var author catalog_service.CreateAuthor

	err := c.ShouldBindJSON(&author)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.AuthorService().Create(
		c.Request.Context(),
		&author,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetAuthorByID godoc
// @ID get_author_by_id
// @Router /author/{id} [GET]
// @Summary Get Author  By ID
// @Description Get Author  By ID
// @Tags Author
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=catalog_service.Author} "AuthorBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAuthorByID(c *gin.Context) {

	authorId := c.Param("id")

	if !util.IsValidUUID(authorId) {
		h.handleResponse(c, http.InvalidArgument, "author id is an invalid uuid")
		return
	}

	resp, err := h.services.AuthorService().GetByID(
		context.Background(),
		&catalog_service.AuthorPrimaryKey{
			Id: authorId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetAuthorList godoc
// @ID get_author_list
// @Router /author [GET]
// @Summary Get Author s List
// @Description  Get Author s List
// @Tags Author
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=catalog_service.GetListAuthorResponse} "GetAllAuthorResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetAuthorList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.AuthorService().GetList(
		context.Background(),
		&catalog_service.GetListAuthorRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateAuthor godoc
// @ID update_author
// @Router /author/{id} [PUT]
// @Summary Update Author
// @Description Update Author
// @Tags Author
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body catalog_service.UpdateAuthor true "UpdateAuthorRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Author} "Author data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateAuthor(c *gin.Context) {

	var authorId catalog_service.UpdateAuthor

	authorId.Id = c.Param("id")

	if !util.IsValidUUID(authorId.Id) {
		h.handleResponse(c, http.InvalidArgument, "author id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&authorId)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.AuthorService().Update(
		c.Request.Context(),
		&authorId,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchAuthor godoc
// @ID patch_author
// @Router /author/{id} [PATCH]
// @Summary Patch Author
// @Description Patch Author
// @Tags Author
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Author} "Author data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchAuthor(c *gin.Context) {

	var updatePatchAuthor models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchAuthor)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchAuthor.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchAuthor.ID) {
		h.handleResponse(c, http.InvalidArgument, "author id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchAuthor.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.AuthorService().UpdatePatch(
		c.Request.Context(),
		&catalog_service.UpdatePatchAuthor{
			Id:     updatePatchAuthor.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteAuthor godoc
// @ID delete_author
// @Router /author/{id} [DELETE]
// @Summary Delete Author
// @Description Delete Author
// @Tags Author
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Author data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteAuthor(c *gin.Context) {

	authorId := c.Param("id")

	if !util.IsValidUUID(authorId) {
		h.handleResponse(c, http.InvalidArgument, "author id is an invalid uuid")
		return
	}

	resp, err := h.services.AuthorService().Delete(
		c.Request.Context(),
		&catalog_service.AuthorPrimaryKey{Id: authorId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
