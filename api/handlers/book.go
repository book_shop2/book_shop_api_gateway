package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"api_gateway/api/http"
	"api_gateway/api/models"
	"api_gateway/genproto/catalog_service"
	"api_gateway/pkg/helper"
	"api_gateway/pkg/util"
)

// CreateBook godoc
// @ID create_book
// @Router /book [POST]
// @Summary Create Book
// @Description  Create Book
// @Tags Book
// @Accept json
// @Produce json
// @Param profile body catalog_service.CreateBook true "CreateBookRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Book} "GetBookBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBook(c *gin.Context) {

	var book catalog_service.CreateBook

	err := c.ShouldBindJSON(&book)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.BookService().Create(
		c.Request.Context(),
		&book,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetBookByID godoc
// @ID get_book_by_id
// @Router /book/{id} [GET]
// @Summary Get Book  By ID
// @Description Get Book  By ID
// @Tags Book
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=catalog_service.Book} "BookBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBookByID(c *gin.Context) {

	bookID := c.Param("id")

	if !util.IsValidUUID(bookID) {
		h.handleResponse(c, http.InvalidArgument, "book id is an invalid uuid")
		return
	}

	resp, err := h.services.BookService().GetByID(
		context.Background(),
		&catalog_service.BookPrimaryKey{
			Id: bookID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetBookList godoc
// @ID get_book_list
// @Router /book [GET]
// @Summary Get Book s List
// @Description  Get Book s List
// @Tags Book
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=catalog_service.GetListBookResponse} "GetAllBookResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBookList(c *gin.Context) {

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.BookService().GetList(
		context.Background(),
		&catalog_service.GetListBookRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateBook godoc
// @ID update_book
// @Router /book/{id} [PUT]
// @Summary Update Book
// @Description Update Book
// @Tags Book
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body catalog_service.UpdateBook true "UpdateBookRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Book} "Book data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBook(c *gin.Context) {

	var bookId catalog_service.UpdateBook

	bookId.Id = c.Param("id")

	if !util.IsValidUUID(bookId.Id) {
		h.handleResponse(c, http.InvalidArgument, "book id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&bookId)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.BookService().Update(
		c.Request.Context(),
		&bookId,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// PatchBook godoc
// @ID patch_book
// @Router /book/{id} [PATCH]
// @Summary Patch Book
// @Description Patch Book
// @Tags Book
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body models.UpdatePatch true "UpdatePatchRequestBody"
// @Success 200 {object} http.Response{data=catalog_service.Book} "Book data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePatchBook(c *gin.Context) {

	var updatePatchBook models.UpdatePatch

	err := c.ShouldBindJSON(&updatePatchBook)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	updatePatchBook.ID = c.Param("id")

	if !util.IsValidUUID(updatePatchBook.ID) {
		h.handleResponse(c, http.InvalidArgument, "book id is an invalid uuid")
		return
	}

	structData, err := helper.ConvertMapToStruct(updatePatchBook.Data)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.BookService().UpdatePatch(
		c.Request.Context(),
		&catalog_service.UpdatePatchBook{
			Id:     updatePatchBook.ID,
			Fields: structData,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteBook godoc
// @ID delete_book
// @Router /book/{id} [DELETE]
// @Summary Delete Book
// @Description Delete Book
// @Tags Book
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Book data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBook(c *gin.Context) {

	bookId := c.Param("id")

	if !util.IsValidUUID(bookId) {
		h.handleResponse(c, http.InvalidArgument, "book id is an invalid uuid")
		return
	}

	resp, err := h.services.BookService().Delete(
		c.Request.Context(),
		&catalog_service.BookPrimaryKey{Id: bookId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
