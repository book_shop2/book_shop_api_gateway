package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"api_gateway/api/docs"
	"api_gateway/api/handlers"
	"api_gateway/config"
)

// New
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func SetUpAPI(r *gin.Engine, h handlers.Handler, cfg config.Config) {
	docs.SwaggerInfo.Title = cfg.ServiceName
	docs.SwaggerInfo.Version = cfg.Version
	docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	r.GET("/config", h.GetConfig)

	r.POST("/category", h.CreateCategory)
	r.GET("/category/:id", h.GetCategoryByID)
	r.GET("/category", h.GetCategoryList)
	r.PUT("/category/:id", h.UpdateCategory)
	r.PATCH("/category/:id", h.UpdatePatchCategory)
	r.DELETE("/category/:id", h.DeleteCategory)

	r.POST("/book", h.CreateBook)
	r.GET("/book/:id", h.GetBookByID)
	r.GET("/book", h.GetBookList)
	r.PUT("/book/:id", h.UpdateBook)
	r.PATCH("/book/:id", h.UpdatePatchBook)
	r.DELETE("/book/:id", h.DeleteBook)

	r.POST("/author", h.CreateAuthor)
	r.GET("/author/:id", h.GetAuthorByID)
	r.GET("/author", h.GetAuthorList)
	r.PUT("/author/:id", h.UpdateAuthor)
	r.PATCH("/author/:id", h.UpdatePatchAuthor)
	r.DELETE("/author/:id", h.DeleteAuthor)

	r.POST("/book_category", h.CreateBook_Category)
	r.GET("/book_category/:id", h.GetBook_CategoryByID)
	r.GET("/book_category", h.GetBook_CategoryList)
	r.PUT("/book_category/:id", h.UpdateBook_Category)
	r.PATCH("/book_category/:id", h.UpdatePatchBook_Category)
	r.DELETE("/book_category/:id", h.DeleteBook_Category)

	r.POST("/order", h.CreateOrder)
	r.GET("/order/:id", h.GetOrderByID)
	r.GET("/order", h.GetOrderList)
	r.PUT("/order/:id", h.UpdateOrder)
	r.PATCH("/order/:id", h.UpdatePatchOrder)
	r.DELETE("/order/:id", h.DeleteOrder)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}
