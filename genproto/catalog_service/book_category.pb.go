// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.12.4
// source: book_category.proto

package catalog_service

import (
	_struct "github.com/golang/protobuf/ptypes/struct"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type BookCategoryPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId string `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
}

func (x *BookCategoryPrimaryKey) Reset() {
	*x = BookCategoryPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BookCategoryPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BookCategoryPrimaryKey) ProtoMessage() {}

func (x *BookCategoryPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BookCategoryPrimaryKey.ProtoReflect.Descriptor instead.
func (*BookCategoryPrimaryKey) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{0}
}

func (x *BookCategoryPrimaryKey) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

type BookCategory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId     string `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id"`
	CreatedAt  string `protobuf:"bytes,3,opt,name=created_at,json=createdAt,proto3" json:"created_at"`
	UpdatedAt  string `protobuf:"bytes,4,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at"`
}

func (x *BookCategory) Reset() {
	*x = BookCategory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BookCategory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BookCategory) ProtoMessage() {}

func (x *BookCategory) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BookCategory.ProtoReflect.Descriptor instead.
func (*BookCategory) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{1}
}

func (x *BookCategory) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *BookCategory) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *BookCategory) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *BookCategory) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateBookCategory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId     string `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id"`
}

func (x *CreateBookCategory) Reset() {
	*x = CreateBookCategory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBookCategory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBookCategory) ProtoMessage() {}

func (x *CreateBookCategory) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBookCategory.ProtoReflect.Descriptor instead.
func (*CreateBookCategory) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{2}
}

func (x *CreateBookCategory) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *CreateBookCategory) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

type DeleteBookCategory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId     string `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id"`
}

func (x *DeleteBookCategory) Reset() {
	*x = DeleteBookCategory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *DeleteBookCategory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*DeleteBookCategory) ProtoMessage() {}

func (x *DeleteBookCategory) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use DeleteBookCategory.ProtoReflect.Descriptor instead.
func (*DeleteBookCategory) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{3}
}

func (x *DeleteBookCategory) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *DeleteBookCategory) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

type UpdateBookCategory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId         string `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	FromCategoryId string `protobuf:"bytes,2,opt,name=from_category_id,json=fromCategoryId,proto3" json:"from_category_id"`
	ToCategoryId   string `protobuf:"bytes,3,opt,name=to_category_id,json=toCategoryId,proto3" json:"to_category_id"`
}

func (x *UpdateBookCategory) Reset() {
	*x = UpdateBookCategory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateBookCategory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateBookCategory) ProtoMessage() {}

func (x *UpdateBookCategory) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateBookCategory.ProtoReflect.Descriptor instead.
func (*UpdateBookCategory) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{4}
}

func (x *UpdateBookCategory) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *UpdateBookCategory) GetFromCategoryId() string {
	if x != nil {
		return x.FromCategoryId
	}
	return ""
}

func (x *UpdateBookCategory) GetToCategoryId() string {
	if x != nil {
		return x.ToCategoryId
	}
	return ""
}

type UpdatePatchBookCategory struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId string          `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	Fields *_struct.Struct `protobuf:"bytes,2,opt,name=fields,proto3" json:"fields"`
}

func (x *UpdatePatchBookCategory) Reset() {
	*x = UpdatePatchBookCategory{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePatchBookCategory) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePatchBookCategory) ProtoMessage() {}

func (x *UpdatePatchBookCategory) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePatchBookCategory.ProtoReflect.Descriptor instead.
func (*UpdatePatchBookCategory) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{5}
}

func (x *UpdatePatchBookCategory) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *UpdatePatchBookCategory) GetFields() *_struct.Struct {
	if x != nil {
		return x.Fields
	}
	return nil
}

type GetListBookCategoryRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search"`
}

func (x *GetListBookCategoryRequest) Reset() {
	*x = GetListBookCategoryRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListBookCategoryRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListBookCategoryRequest) ProtoMessage() {}

func (x *GetListBookCategoryRequest) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListBookCategoryRequest.ProtoReflect.Descriptor instead.
func (*GetListBookCategoryRequest) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{6}
}

func (x *GetListBookCategoryRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListBookCategoryRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListBookCategoryRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListBookCategoryResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count int64               `protobuf:"varint,1,opt,name=count,proto3" json:"count"`
	Books []*BookCategoryList `protobuf:"bytes,2,rep,name=books,proto3" json:"books"`
}

func (x *GetListBookCategoryResponse) Reset() {
	*x = GetListBookCategoryResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListBookCategoryResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListBookCategoryResponse) ProtoMessage() {}

func (x *GetListBookCategoryResponse) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListBookCategoryResponse.ProtoReflect.Descriptor instead.
func (*GetListBookCategoryResponse) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{7}
}

func (x *GetListBookCategoryResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListBookCategoryResponse) GetBooks() []*BookCategoryList {
	if x != nil {
		return x.Books
	}
	return nil
}

type BookCategoryList struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BookId         string           `protobuf:"bytes,1,opt,name=book_id,json=bookId,proto3" json:"book_id"`
	BookTitle      string           `protobuf:"bytes,2,opt,name=book_title,json=bookTitle,proto3" json:"book_title"`
	AuthorName     string           `protobuf:"bytes,3,opt,name=author_name,json=authorName,proto3" json:"author_name"`
	CreatedAt      string           `protobuf:"bytes,4,opt,name=created_at,json=createdAt,proto3" json:"created_at"`
	UpdatedAt      string           `protobuf:"bytes,5,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at"`
	BookCategories []*CategoryLists `protobuf:"bytes,6,rep,name=book_categories,json=bookCategories,proto3" json:"book_categories"`
}

func (x *BookCategoryList) Reset() {
	*x = BookCategoryList{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BookCategoryList) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BookCategoryList) ProtoMessage() {}

func (x *BookCategoryList) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BookCategoryList.ProtoReflect.Descriptor instead.
func (*BookCategoryList) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{8}
}

func (x *BookCategoryList) GetBookId() string {
	if x != nil {
		return x.BookId
	}
	return ""
}

func (x *BookCategoryList) GetBookTitle() string {
	if x != nil {
		return x.BookTitle
	}
	return ""
}

func (x *BookCategoryList) GetAuthorName() string {
	if x != nil {
		return x.AuthorName
	}
	return ""
}

func (x *BookCategoryList) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *BookCategoryList) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

func (x *BookCategoryList) GetBookCategories() []*CategoryLists {
	if x != nil {
		return x.BookCategories
	}
	return nil
}

type CategoryLists struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id"`
	Name       string `protobuf:"bytes,2,opt,name=name,proto3" json:"name"`
	ParentName string `protobuf:"bytes,3,opt,name=parent_name,json=parentName,proto3" json:"parent_name"`
	CreatedAt  string `protobuf:"bytes,4,opt,name=created_at,json=createdAt,proto3" json:"created_at"`
	UpdatedAt  string `protobuf:"bytes,5,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at"`
}

func (x *CategoryLists) Reset() {
	*x = CategoryLists{}
	if protoimpl.UnsafeEnabled {
		mi := &file_book_category_proto_msgTypes[9]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CategoryLists) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CategoryLists) ProtoMessage() {}

func (x *CategoryLists) ProtoReflect() protoreflect.Message {
	mi := &file_book_category_proto_msgTypes[9]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CategoryLists.ProtoReflect.Descriptor instead.
func (*CategoryLists) Descriptor() ([]byte, []int) {
	return file_book_category_proto_rawDescGZIP(), []int{9}
}

func (x *CategoryLists) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *CategoryLists) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CategoryLists) GetParentName() string {
	if x != nil {
		return x.ParentName
	}
	return ""
}

func (x *CategoryLists) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *CategoryLists) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

var File_book_category_proto protoreflect.FileDescriptor

var file_book_category_proto_rawDesc = []byte{
	0x0a, 0x13, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0f, 0x63, 0x61, 0x74, 0x61, 0x6c, 0x6f, 0x67, 0x5f, 0x73,
	0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1c, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0x31, 0x0a, 0x16, 0x42, 0x6f, 0x6f, 0x6b, 0x43, 0x61, 0x74, 0x65,
	0x67, 0x6f, 0x72, 0x79, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x17,
	0x0a, 0x07, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x06, 0x62, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x22, 0x86, 0x01, 0x0a, 0x0c, 0x42, 0x6f, 0x6f, 0x6b,
	0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x62, 0x6f, 0x6f, 0x6b,
	0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f, 0x6f, 0x6b, 0x49,
	0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79,
	0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x22, 0x4e, 0x0a, 0x12, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x6f, 0x6f, 0x6b, 0x43, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x12,
	0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64,
	0x22, 0x4e, 0x0a, 0x12, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x42, 0x6f, 0x6f, 0x6b, 0x43, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x12,
	0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64,
	0x22, 0x7d, 0x0a, 0x12, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x6f, 0x6f, 0x6b, 0x43, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f, 0x6f, 0x6b, 0x49, 0x64, 0x12,
	0x28, 0x0a, 0x10, 0x66, 0x72, 0x6f, 0x6d, 0x5f, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x66, 0x72, 0x6f, 0x6d, 0x43,
	0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12, 0x24, 0x0a, 0x0e, 0x74, 0x6f, 0x5f,
	0x63, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0c, 0x74, 0x6f, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x22,
	0x63, 0x0a, 0x17, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x74, 0x63, 0x68, 0x42, 0x6f,
	0x6f, 0x6b, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x12, 0x17, 0x0a, 0x07, 0x62, 0x6f,
	0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f, 0x6f,
	0x6b, 0x49, 0x64, 0x12, 0x2f, 0x0a, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74, 0x52, 0x06, 0x66, 0x69,
	0x65, 0x6c, 0x64, 0x73, 0x22, 0x62, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x42,
	0x6f, 0x6f, 0x6b, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x52, 0x65, 0x71, 0x75, 0x65,
	0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69,
	0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x6c, 0x0a, 0x1b, 0x47, 0x65, 0x74, 0x4c,
	0x69, 0x73, 0x74, 0x42, 0x6f, 0x6f, 0x6b, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x37, 0x0a,
	0x05, 0x62, 0x6f, 0x6f, 0x6b, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x21, 0x2e, 0x63,
	0x61, 0x74, 0x61, 0x6c, 0x6f, 0x67, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x42,
	0x6f, 0x6f, 0x6b, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4c, 0x69, 0x73, 0x74, 0x52,
	0x05, 0x62, 0x6f, 0x6f, 0x6b, 0x73, 0x22, 0xf2, 0x01, 0x0a, 0x10, 0x42, 0x6f, 0x6f, 0x6b, 0x43,
	0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4c, 0x69, 0x73, 0x74, 0x12, 0x17, 0x0a, 0x07, 0x62,
	0x6f, 0x6f, 0x6b, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x62, 0x6f,
	0x6f, 0x6b, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x74, 0x69, 0x74,
	0x6c, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x62, 0x6f, 0x6f, 0x6b, 0x54, 0x69,
	0x74, 0x6c, 0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72, 0x5f, 0x6e, 0x61,
	0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x61, 0x75, 0x74, 0x68, 0x6f, 0x72,
	0x4e, 0x61, 0x6d, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f,
	0x61, 0x74, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61,
	0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x41, 0x74, 0x12, 0x47, 0x0a, 0x0f, 0x62, 0x6f, 0x6f, 0x6b, 0x5f, 0x63, 0x61, 0x74, 0x65, 0x67,
	0x6f, 0x72, 0x69, 0x65, 0x73, 0x18, 0x06, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1e, 0x2e, 0x63, 0x61,
	0x74, 0x61, 0x6c, 0x6f, 0x67, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x43, 0x61,
	0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4c, 0x69, 0x73, 0x74, 0x73, 0x52, 0x0e, 0x62, 0x6f, 0x6f,
	0x6b, 0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x69, 0x65, 0x73, 0x22, 0x92, 0x01, 0x0a, 0x0d,
	0x43, 0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x4c, 0x69, 0x73, 0x74, 0x73, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x61, 0x72, 0x65, 0x6e, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x61, 0x72, 0x65, 0x6e, 0x74, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41,
	0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x42, 0x1a, 0x5a, 0x18, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x63, 0x61, 0x74,
	0x61, 0x6c, 0x6f, 0x67, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_book_category_proto_rawDescOnce sync.Once
	file_book_category_proto_rawDescData = file_book_category_proto_rawDesc
)

func file_book_category_proto_rawDescGZIP() []byte {
	file_book_category_proto_rawDescOnce.Do(func() {
		file_book_category_proto_rawDescData = protoimpl.X.CompressGZIP(file_book_category_proto_rawDescData)
	})
	return file_book_category_proto_rawDescData
}

var file_book_category_proto_msgTypes = make([]protoimpl.MessageInfo, 10)
var file_book_category_proto_goTypes = []interface{}{
	(*BookCategoryPrimaryKey)(nil),      // 0: catalog_service.BookCategoryPrimaryKey
	(*BookCategory)(nil),                // 1: catalog_service.BookCategory
	(*CreateBookCategory)(nil),          // 2: catalog_service.CreateBookCategory
	(*DeleteBookCategory)(nil),          // 3: catalog_service.DeleteBookCategory
	(*UpdateBookCategory)(nil),          // 4: catalog_service.UpdateBookCategory
	(*UpdatePatchBookCategory)(nil),     // 5: catalog_service.UpdatePatchBookCategory
	(*GetListBookCategoryRequest)(nil),  // 6: catalog_service.GetListBookCategoryRequest
	(*GetListBookCategoryResponse)(nil), // 7: catalog_service.GetListBookCategoryResponse
	(*BookCategoryList)(nil),            // 8: catalog_service.BookCategoryList
	(*CategoryLists)(nil),               // 9: catalog_service.CategoryLists
	(*_struct.Struct)(nil),              // 10: google.protobuf.Struct
}
var file_book_category_proto_depIdxs = []int32{
	10, // 0: catalog_service.UpdatePatchBookCategory.fields:type_name -> google.protobuf.Struct
	8,  // 1: catalog_service.GetListBookCategoryResponse.books:type_name -> catalog_service.BookCategoryList
	9,  // 2: catalog_service.BookCategoryList.book_categories:type_name -> catalog_service.CategoryLists
	3,  // [3:3] is the sub-list for method output_type
	3,  // [3:3] is the sub-list for method input_type
	3,  // [3:3] is the sub-list for extension type_name
	3,  // [3:3] is the sub-list for extension extendee
	0,  // [0:3] is the sub-list for field type_name
}

func init() { file_book_category_proto_init() }
func file_book_category_proto_init() {
	if File_book_category_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_book_category_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BookCategoryPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BookCategory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateBookCategory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*DeleteBookCategory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateBookCategory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePatchBookCategory); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListBookCategoryRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListBookCategoryResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BookCategoryList); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_book_category_proto_msgTypes[9].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CategoryLists); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_book_category_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   10,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_book_category_proto_goTypes,
		DependencyIndexes: file_book_category_proto_depIdxs,
		MessageInfos:      file_book_category_proto_msgTypes,
	}.Build()
	File_book_category_proto = out.File
	file_book_category_proto_rawDesc = nil
	file_book_category_proto_goTypes = nil
	file_book_category_proto_depIdxs = nil
}
