package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"api_gateway/config"
	"api_gateway/genproto/catalog_service"
	"api_gateway/genproto/order_service"
)

type ServiceManagerI interface {
	CategoryService() catalog_service.CategoryServiceClient
	BookService() catalog_service.BookServiceClient
	AuthorService() catalog_service.AuthorServiceClient
	Book_CategoryService() catalog_service.BookCategoryServiceClient
	OrderService() order_service.OrderServiceClient
}

type grpcClients struct {
	categoryService      catalog_service.CategoryServiceClient
	bookService          catalog_service.BookServiceClient
	authorService        catalog_service.AuthorServiceClient
	book_categoryService catalog_service.BookCategoryServiceClient
	orderService         order_service.OrderServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	// Order Service...
	connOrderService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.OrderGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		categoryService:      catalog_service.NewCategoryServiceClient(connUserService),
		bookService:          catalog_service.NewBookServiceClient(connUserService),
		authorService:        catalog_service.NewAuthorServiceClient(connUserService),
		book_categoryService: catalog_service.NewBookCategoryServiceClient(connUserService),
		orderService:         order_service.NewOrderServiceClient(connOrderService),
	}, nil
}

func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) BookService() catalog_service.BookServiceClient {
	return g.bookService
}

func (g *grpcClients) AuthorService() catalog_service.AuthorServiceClient {
	return g.authorService
}

func (g *grpcClients) Book_CategoryService() catalog_service.BookCategoryServiceClient {
	return g.book_categoryService
}

func (g *grpcClients) OrderService() order_service.OrderServiceClient {
	return g.orderService
}
