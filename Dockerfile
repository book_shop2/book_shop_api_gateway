FROM golang:1.19-alpine AS builder

# git is required to fetch go dependencies
RUN apk add --no-cache ca-certificates git
# Create the user and group files that will be used in the running 
# container to run the process as an unprivileged user.
RUN mkdir /user && \
    echo 'nobody:x:65534:65534:nobody:/:' > /user/passwd && \
    echo 'nobody:x:65534:' > /user/group
# Copy the predefined netrc file into the location that git depends on
COPY ./netrc /root/.netrc
RUN chmod 600 /root/.netrc


WORKDIR /src


COPY go.mod ./
COPY go.sum ./
RUN go mod download 

COPY . ./

WORKDIR ./cmd

RUN go build -o /app

EXPOSE 8001

# Final stage: the running container.
FROM scratch AS final
# Import the user and group files from the first stage.
COPY --from=builder /user/group /user/passwd /etc/
# Import the Certificate-Authority certificates for enabling HTTPS.
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Import the compiled executable from the first stage.
COPY --from=builder /app /app
# Perform any further action as an unprivileged user.
USER nobody:nobody
# Run the compiled binary.
ENTRYPOINT ["/app"]

CMD ["/app"]