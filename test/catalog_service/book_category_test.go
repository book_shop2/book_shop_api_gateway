package catalog_service

import (
	"fmt"
	"net/http"
	"sync"
	"testing"

	"api_gateway/genproto/catalog_service"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

func TestAddBook_Categories(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 10000; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createBook_Category(t)
			updateBook_Category(t, id)
			deleteBook_Category(t, id)

		}()
	}

	wg.Wait()

}

func createBook_Category(t *testing.T) string {

	response := catalog_service.BookCategoryPrimaryKey{}

	request := &catalog_service.CreateBookCategory{
		BookId:     faker.UUIDDigit(),
		CategoryId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book_category", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.BookId
}

func updateBook_Category(t *testing.T, id string) string {

	response := catalog_service.BookCategoryPrimaryKey{}

	request := &catalog_service.UpdateBookCategory{
		BookId:         faker.UUIDDigit(),
		FromCategoryId: faker.UUIDDigit(),
		ToCategoryId:   faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book_category"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.BookId
}

func deleteBook_Category(t *testing.T, id string) string {

	response := catalog_service.BookCategoryPrimaryKey{}

	request := &catalog_service.BookCategoryPrimaryKey{
		BookId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book_category"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.BookId
}
