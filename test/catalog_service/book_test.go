package catalog_service

import (
	"fmt"
	"net/http"
	"sync"
	"testing"

	"api_gateway/genproto/catalog_service"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

func TestAddBooks(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 10000; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createBook(t)
			updateBook(t, id)
			deleteBook(t, id)

		}()
	}

	wg.Wait()

}

func createBook(t *testing.T) string {

	response := catalog_service.BookPrimaryKey{}

	request := &catalog_service.CreateBook{
		Name:     faker.Name(),
		AuthorId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func updateBook(t *testing.T, id string) string {

	response := catalog_service.BookPrimaryKey{}

	request := &catalog_service.UpdateBook{
		Id:       faker.UUIDDigit(),
		Name:     faker.Name(),
		AuthorId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func deleteBook(t *testing.T, id string) string {

	response := catalog_service.BookPrimaryKey{}

	request := &catalog_service.BookPrimaryKey{
		Id: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/book"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}
