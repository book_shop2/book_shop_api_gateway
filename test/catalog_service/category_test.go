package catalog_service

import (
	"fmt"
	"net/http"
	"sync"
	"testing"

	"api_gateway/genproto/catalog_service"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

func TestAddCategories(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 10000; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createCategory(t)
			updateCategory(t, id)
			deleteCategory(t, id)

		}()
	}

	wg.Wait()
}

func createCategory(t *testing.T) string {

	response := catalog_service.CategoryPrimaryKey{}

	request := &catalog_service.CreateCategory{
		Name:     faker.Name(),
		ParentId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/category", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func updateCategory(t *testing.T, id string) string {

	response := catalog_service.Category{}

	request := &catalog_service.UpdateCategory{
		Id:       faker.UUIDDigit(),
		Name:     faker.Name(),
		ParentId: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/category/"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func deleteCategory(t *testing.T, id string) string {

	response := catalog_service.Category{}

	request := &catalog_service.CategoryPrimaryKey{
		Id: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/category/"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}
