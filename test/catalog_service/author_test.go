package catalog_service

import (
	"fmt"
	"net/http"
	"sync"
	"testing"

	"api_gateway/genproto/catalog_service"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

func TestAddAuthors(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 10000; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createAuthor(t)
			updateAuthor(t, id)
			deleteAuthor(t, id)

		}()
	}

	wg.Wait()

}

func createAuthor(t *testing.T) string {

	response := catalog_service.AuthorPrimaryKey{}

	request := &catalog_service.CreateAuthor{
		Name: faker.Name(),
	}

	resp, err := PerformRequest(http.MethodPost, "/author", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func updateAuthor(t *testing.T, id string) string {

	response := catalog_service.AuthorPrimaryKey{}

	request := &catalog_service.UpdateAuthor{
		Id:   faker.UUIDDigit(),
		Name: faker.Name(),
	}

	resp, err := PerformRequest(http.MethodPost, "/author"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func deleteAuthor(t *testing.T, id string) string {

	response := catalog_service.AuthorPrimaryKey{}

	request := &catalog_service.AuthorPrimaryKey{
		Id: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/author"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}
