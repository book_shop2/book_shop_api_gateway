package order_service

import (
	"fmt"
	"net/http"
	"sync"
	"testing"

	"api_gateway/genproto/order_service"

	"github.com/bxcodec/faker/v3"
	"github.com/test-go/testify/assert"
)

func TestAddOrders(t *testing.T) {
	wg := &sync.WaitGroup{}

	for i := 0; i < 10000; i++ {

		wg.Add(1)
		go func() {
			defer wg.Done()
			id := createOrder(t)
			updateOrder(t, id)
			deleteOrder(t, id)

		}()
	}

	wg.Wait()

}

func createOrder(t *testing.T) string {

	response := order_service.OrderPrimaryKey{}

	request := &order_service.CreateOrder{
		BookId:      faker.UUIDDigit(),
		Description: faker.Sentence(),
	}

	resp, err := PerformRequest(http.MethodPost, "/order", request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func updateOrder(t *testing.T, id string) string {

	response := order_service.OrderPrimaryKey{}

	request := &order_service.UpdateOrder{
		Id:          faker.UUIDDigit(),
		BookId:      faker.UUIDDigit(),
		Description: faker.Sentence(),
	}

	resp, err := PerformRequest(http.MethodPost, "/order"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}

func deleteOrder(t *testing.T, id string) string {

	response := order_service.OrderPrimaryKey{}

	request := &order_service.OrderPrimaryKey{
		Id: faker.UUIDDigit(),
	}

	resp, err := PerformRequest(http.MethodPost, "/order"+id, request, response)

	assert.NoError(t, err)

	assert.NotNil(t, resp)

	if resp != nil {
		assert.Equal(t, resp.StatusCode, 201)
	}

	fmt.Println(response)

	return response.Id
}
